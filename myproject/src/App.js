import React from 'react';
import { BrowserRouter as Router, Route, Routes, useLocation } from 'react-router-dom'; // Import Routes, not Routes
import RegistrationForm from './Components/Register';
import MyNavbar from './Components/Navbar';
import Login from './Components/Login';
import MedicalStore from './Components/medicine';
import BMI from './Components/bmi';
import HealthMonitor from './Components/Health';
import 'bootstrap/dist/css/bootstrap.min.css';

import Contact from './Components/Contact';
import './App.css';

function Content() {
  const location = useLocation();
  return (
    <div className="App">
      {location.pathname !== '/Register' && location.pathname !== '/Login' &&
      location.pathname !== '/medicine' &&
      location.pathname !== '/Contact' &&
      location.pathname !== '/bmi' &&
      location.pathname !== '/Health' &&
      <MyNavbar />}
      <Routes>
        <Route path="/" element={<div>Homepage content or any default component goes here.</div>} />
        <Route path="/Register" element={<RegistrationForm />} />
        <Route path="/Login" element={<Login />} />
        <Route path="/medicine" element={<MedicalStore />} />
        <Route path="/Contact" element={<Contact />} />
        <Route path="/bmi" element={<BMI />} />
        <Route path="/Health" element={<HealthMonitor />} />
      </Routes>
    </div>
  );
}

function App() {
  return (
    <Router>
      <Routes>
        <Route path="*" element={<Content />} />
      </Routes>
    </Router>
  );
}

export default App;
