
import React, { useState } from 'react';
import { FaStar } from 'react-icons/fa';
import Expert1Image from '../images/images/expert1.jpg';
import Expert2Image from '../images/images/expert2.jpg';
import Expert3Image from '../images/images/expert3.jpg';
import Expert4Image from '../images/images/expert4.jpg';
import './Review.css';

const Reviews = () => {
  const [reviews, setReviews] = useState([]);
  const [newReview, setNewReview] = useState('');
  const [rating, setRating] = useState(0);
  const [isSubmitted, setIsSubmitted] = useState(false);

  const expertsFeedback = [
    {
      image: Expert1Image,
      name: 'Dr. John Doe',
      expertise: 'Cardiologist',
      feedback: 'Exceptional service and genuine products. I highly recommend this online medical store for its dedication to customer health and safety.',
      rating: 5,
    },
    {
      image: Expert2Image,
      name: 'Dr. Jane Smith',
      expertise: 'Pediatrician',
      feedback: 'A reliable platform for all your medical needs. Quick delivery, professional customer support, and a wide range of authentic products.',
      rating: 4,
    },
    {
      image: Expert3Image,
      name: 'Dr. Michael Johnson',
      expertise: 'Dermatologist',
      feedback: 'This online medical store stands out with its excellent customer service and high-quality products. I trust this platform for my patients and my family.',
      rating: 5,
    },
    {
      image: Expert4Image,
      name: 'Dr. Emily Clark',
      expertise: 'Psychiatrist',
      feedback: 'I am impressed with the seamless experience this platform provides. From ordering to delivery, it\'s a hassle-free process. Highly recommended!',
      rating: 5,
    }
  ];

  const handleReviewSubmit = (e) => {
    e.preventDefault();
    if (newReview && rating > 0) {
      const reviewWithRating = `${'⭐'.repeat(rating)} ${newReview}`;
      setReviews([...reviews, reviewWithRating]);
      setNewReview('');
      setRating(0);
      setIsSubmitted(true);
    }
  };

  return (
    <div className="reviews-section">
      {/* Expert Feedback Section */}
      <h2>Expert Feedback</h2>
      {expertsFeedback.map((expert, index) => (
        <div key={index} className="expert-feedback">
          <img src={expert.image} alt={`Expert ${index + 1}`} className="expert-image" />
          <div className="expert-info">
            <h3>{expert.name}</h3>
            <p><strong>Expertise:</strong> {expert.expertise}</p>
            <p>{expert.feedback}</p>
            <div className="rating-stars">
              {[...Array(5)].map((star, i) => (
                <FaStar key={i} color={i < expert.rating ? '#ffc107' : '#e4e5e9'} size={20} />
              ))}
            </div>
          </div>
        </div>
      ))}

      {/* Customer Reviews Section */}
      <h2>Customer Reviews</h2>
      <ul>
        {reviews.map((review, index) => (
          <li key={index}>{review}</li>
        ))}
      </ul>

      <form onSubmit={handleReviewSubmit}>
        <div className="rating-stars">
          {[...Array(5)].map((star, index) => {
            const ratingValue = index + 1;
            return (
              <label key={index}>
                <input
                  type="radio"
                  name="rating"
                  value={ratingValue}
                  onClick={() => setRating(ratingValue)}
                />
                <FaStar
                  className="star"
                  color={ratingValue <= rating ? '#ffc107' : '#e4e5e9'}
                  size={25}
                />
              </label>
            );
          })}
        </div>
        <textarea
          rows="4"
          cols="50"
          placeholder="Write your review here..."
          value={newReview}
          onChange={(e) => setNewReview(e.target.value)}
        ></textarea>
        <button type="submit" className='submit-button'>Submit Review</button>
      </form>
      {isSubmitted && <p>Thanks for your feedback! Your review has been submitted.</p>}
    </div>
  );
};

export default Reviews;
