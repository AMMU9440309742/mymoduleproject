// src/Home.js

import React from 'react';

function Home() {
    return (
        <div className="home">
            <header>
                <h1>Welcome to Our Homepage</h1>
            </header>
            <main>
                <p>This is a simple home page created in React.</p>
            </main>
            <footer>
                <p>© 2023 by Our Company. Proudly created with React.</p>
            </footer>
        </div>
    );
}

export default Home;
