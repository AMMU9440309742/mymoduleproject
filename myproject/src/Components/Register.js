
import React, { useState } from 'react';
import axios from 'axios';
import { FaUser, FaPhone, FaBirthdayCake, FaEnvelope, FaLock, FaEye, FaEyeSlash } from 'react-icons/fa';

import '../Components/Register.css';

function RegistrationForm() {
    const [successMessage, setSuccessMessage] = useState(''); // Initialize successMessage state

    const [formData, setFormData] = useState({
        username: '',
        phoneNumber: '',
        age: '',
        email: '',
        password: '',
        confirmPassword: ''
    });
    const [showPassword, setShowPassword] = useState(false);

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const togglePasswordVisibility = () => {
        setShowPassword(!showPassword);
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post('http://localhost:5000/register', formData);
            console.log('Form data submitted:', response.data);
            setSuccessMessage('Registration successful! Redirecting to login page...');
            setTimeout(() => {
                window.location.href = '/Login'; // Redirect to the login page after 2 seconds
            }, 2000);
        } catch (error) {
            console.error('Error submitting form:', error);
        }
    };

    return (
        <div className="registration-container">
            <h2>Create an Account</h2>
            <form className="registration-form" onSubmit={handleSubmit}>
                {/* Username */}
                <div className="input-container">
                    <FaUser />
                    <input 
                        type="text" 
                        name="username" 
                        placeholder="Username" 
                        value={formData.username} 
                        onChange={handleChange} 
                        required
                    />
                </div>
                {/* Phone Number */}
                <div className="input-container">
                    <FaPhone />
                    <input 
                        type="tel" 
                        name="phoneNumber" 
                        placeholder="Phone Number" 
                        value={formData.phoneNumber} 
                        onChange={handleChange} 
                        required
                    />
                </div>
                {/* Age */}
                <div className="input-container">
                    <FaBirthdayCake />
                    <input 
                        type="number" 
                        name="age" 
                        placeholder="Age" 
                        value={formData.age} 
                        onChange={handleChange} 
                        required
                    />
                </div>
                {/* Email */}
                <div className="input-container">
                    <FaEnvelope />
                    <input 
                        type="email" 
                        name="email" 
                        placeholder="Email" 
                        value={formData.email} 
                        onChange={handleChange} 
                        required
                    />
                </div>
                {/* Password */}
                <div className="input-container">
                    <FaLock />
                    <input 
                        type={showPassword ? "text" : "password"}
                        name="password"
                        placeholder="Password"
                        value={formData.password}
                        onChange={handleChange}
                        required
                    />
                    <div className="toggle-password-icon" onClick={togglePasswordVisibility}>
                        {showPassword ? <FaEyeSlash /> : <FaEye />}
                    </div>
                </div>
                {/* Confirm Password */}
                <div className="input-container">
                    <FaLock />
                    <input 
                        type={showPassword ? "text" : "password"}
                        name="confirmPassword"
                        placeholder="Confirm Password"
                        value={formData.confirmPassword}
                        onChange={handleChange}
                        required
                    />
                    <div className="toggle-password-icon" onClick={togglePasswordVisibility}>
                        {showPassword ? <FaEyeSlash /> : <FaEye />}
                    </div>
                </div>
                <button type="submit">Register</button>
            </form>
            {/* <div className="form-footer">
                <p>Already have an account? <Link to="/login">Log in</Link></p>
                <p>Terms & Conditions | <Link to="/privacy">Privacy Policy</Link></p>
            </div> */}
        </div>
    );
}

export default RegistrationForm;
