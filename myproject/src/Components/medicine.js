
import React, { useState } from 'react';
import {
    Button,
    Modal,
    ModalHeader,
    ModalBody,
    ModalFooter,
    Form,
    FormGroup,
    Label,
    Input,
    Carousel,
    CarouselItem,
    CarouselControl,
    CarouselIndicators
} from 'reactstrap';
import { FaStar } from 'react-icons/fa';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../Components/medicine.css';
import '../Components/Register.css';
import Paracetamol from '../images/images/paracetemol.jpg';
import coughsyrup from '../images/images/coughsyrup.jpg';
import Vitamin from '../images/images/vitaminc.jpg';
import ibuprofen from '../images/images/Ibuprofen.jpg';
import Lipitor from '../images/images/Liptor.jpg';
import Amlodipine from '../images/images/Amlodipine.jpg';
import Hydrochlorothiazide from '../images/images/Hydrochlorothiazide.jpg';
import Lisinopril from '../images/images/Lisinopril.jpg';
import Omeprazole from '../images/images/Omeprazole.jpg';
import Losartan from '../images/images/Losartan.jpg';
import Meloxicam from '../images/images/Meloxicam.jpg';
import C1 from '../images/images/c1.jpg';
import C3 from '../images/images/c3.jpg';
import C5 from '../images/images/c5.jpg';
import Card1 from '../images/images/card1.jpg';
import card2 from '../images/images/card2.jpg';
import card3 from '../images/images/card3.jpg';
import card4 from '../images/images/card4.jpg';
import card5 from '../images/images/card5.jpg';
import card6 from '../images/images/card6.jpg';
import card7 from '../images/images/card7.jpg';
import card8 from '../images/images/card8.jpg';
import card9 from '../images/images/card9.jpg';
import card10 from '../images/images/card10.jpg';
import Reviews from './Reviews.js';

const medicines = [
    { id: 1, name: 'Paracetamol', image: Paracetamol },
    { id: 2, name: 'Cough Syrup', image: coughsyrup },
    { id: 3, name: 'Vitamin C', image: Vitamin },
    { id: 4, name: 'Ibuprofen', image: ibuprofen },
    { id: 5, name: 'Lipitor', image: Lipitor },
    { id: 6, name: 'Amlodipine', image: Amlodipine },
    { id: 7, name: 'Hydrochlorothiazide', image: Hydrochlorothiazide },
    { id: 8, name: 'Lisinopril', image: Lisinopril },
    { id: 9, name: 'Omeprazole', image: Omeprazole },
    { id: 10, name: 'Losartan', image: Losartan },
    { id: 11, name: 'Meloxicam', image: Meloxicam }
];

const cardImages = [Card1, card2, card3, card4, card5, card6, card7, card8, card9, card10];

const carouselImages = [C1, C3, C5];

const MedicalStore = () => {
    const [searchTerm, setSearchTerm] = useState('');
    const [modal, setModal] = useState(false);
    const [selectedMedicine, setSelectedMedicine] = useState('');
    const [ratings, setRatings] = useState({});
    const [orderDetails, setOrderDetails] = useState({
        name: '',
        email: '',
        phoneNumber: '',
        address: '',
        quantity: '',
    });
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === carouselImages.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    };

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? carouselImages.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    };

    const goToIndex = newIndex => {
        if (animating) return;
        setActiveIndex(newIndex);
    };

    const handleOrderNow = medicineName => {
        setSelectedMedicine(medicineName);
        toggle();
    };

    const handleRatingChange = (medicineName, rating) => {
        setRatings({ ...ratings, [medicineName]: rating });
    };

    const handleInputChange = e => {
        const { name, value } = e.target;
        setOrderDetails({ ...orderDetails, [name]: value });
    };

    
    const resetForm = () => {
        setOrderDetails({
            name: '',
            email: '',
            phoneNumber: '',
            address: '',
            quantity: 1
        });
    };

   


    const [isOrderConfirmed, setOrderConfirmed] = useState(false);

    const toggle = () => {
        resetForm();
        // Reset the isOrderConfirmed state to false when the modal is opened
        setOrderConfirmed(false);
        // Toggle the modal
        setModal(!modal);
    };

    const handleConfirmOrder = () => {
        // Perform order confirmation logic here
        // For example, you can send an API request to the server to confirm the order
        // Assuming the order confirmation is successful, set the orderConfirmed state to true
        setOrderConfirmed(true);
    };
    const slides = carouselImages.map((image, index) => (
        <CarouselItem onExiting={() => setAnimating(true)} onExited={() => setAnimating(false)} key={index}>
            <img src={image} alt={`slide ${index}`} width="100%" height="500px" />
        </CarouselItem>
    ));

    return (
        <div>
            {/* Image Cards before Carousel */}
            <div className="image-cards-container">
                {cardImages.map((image, index) => (
                    <div key={index} className="image-card">
                        <img src={image} alt={`card-image-${index}`} />
                    </div>
                ))}
            </div>

            {/* Carousel */}
            <Carousel activeIndex={activeIndex} next={next} previous={previous} ride="carousel" interval={2000}>
                <CarouselIndicators items={carouselImages} activeIndex={activeIndex} onClickHandler={goToIndex} />
                {slides}
                <CarouselControl direction="prev" directionText="Previous" onClickHandler={previous} />
                <CarouselControl direction="next" directionText="Next" onClickHandler={next} />
            </Carousel>

            {/* Search input */}
            <div className="search-container">
                <input
                    className="search-input"
                    type="text"
                    placeholder="Search here for a medicine"
                    value={searchTerm}
                    onChange={e => setSearchTerm(e.target.value)}
                />
                <i className="fa fa-search search-icon"></i>
            </div>

            {/* Medicine Cards */}
            <div className="medicine-cards">
                {medicines
                    .filter(medicine => medicine.name.toLowerCase().includes(searchTerm.toLowerCase()))
                    .map(filteredMedicine => (
                        <div key={filteredMedicine.id} className="medicine-card">
                            <img src={filteredMedicine.image} alt={filteredMedicine.name} />
                            <p>{filteredMedicine.name}</p>
                            <Button color="primary" onClick={() => handleOrderNow(filteredMedicine.name)}>
                                Order Now
                            </Button>
                            <div className="rating-stars">
                                {[...Array(5)].map((_, index) => (
                                    <FaStar
                                        key={index}
                                        color={index + 1 <= (ratings[filteredMedicine.name] || 0) ? '#ffc107' : '#e4e5e9'}
                                        size={20}
                                        onClick={() => handleRatingChange(filteredMedicine.name, index + 1)}
                                    />
                                ))}
                            </div>
                        </div>
                    ))}
            </div>

            {/* Order Modal */}
            <Modal isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>Order {selectedMedicine && selectedMedicine}</ModalHeader>
                <ModalBody>
                    <Form>
                        <FormGroup>
                            <Label for="name">Full Name</Label>
                            <Input
                                type="text"
                                name="name"
                                id="name"
                                placeholder="Enter your full name"
                                value={orderDetails.name}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="email">Email</Label>
                            <Input
                                type="email"
                                name="email"
                                id="email"
                                placeholder="Enter your email"
                                value={orderDetails.email}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="phoneNumber">Phone Number</Label>
                            <Input
                                type="tel"
                                name="phoneNumber"
                                id="phoneNumber"
                                placeholder="Enter your phone number"
                                value={orderDetails.phoneNumber}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="address">Address</Label>
                            <Input
                                type="text"
                                name="address"
                                id="address"
                                placeholder="Enter your address"
                                value={orderDetails.address}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                        <FormGroup>
                            <Label for="quantity">Quantity</Label>
                            <Input
                                type="number"
                                name="quantity"
                                id="quantity"
                                min="1"
                                value={orderDetails.quantity}
                                onChange={handleInputChange}
                            />
                        </FormGroup>
                    </Form>
                </ModalBody>
                <ModalFooter>
                <Button color="primary" onClick={() => { toggle(); handleConfirmOrder(); }}>
                    Confirm Order
                </Button>{' '}
                <Button color="secondary" onClick={() => { toggle(); resetForm(); }}>
                    Cancel
                </Button>
            </ModalFooter>

            {/* Success Message */}
            {isOrderConfirmed && (
                <div className="success-message">
                    <p>Order placed successfully!</p>
                </div>
            )}
            </Modal>

            {/* Reviews Component */}
            <Reviews />

            {/* Footer */}
            <footer className="store-footer">
                <div className="footer-content">
                    {/* Footer content */}
                </div>
            </footer>
        </div>
    );
};

export default MedicalStore;

            
