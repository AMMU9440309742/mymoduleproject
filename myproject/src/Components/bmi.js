
import React, { useState } from 'react';
import './bmi.css';

const BMICalculator = () => {
  const [height, setHeight] = useState('');
  const [weight, setWeight] = useState('');
  const [bmi, setBMI] = useState(null);
  const [bmiCategory, setBMICategory] = useState('');

  const calculateBMI = () => {
    if (height && weight) {
      const heightInMeters = height / 100;
      const bmiValue = (weight / (heightInMeters * heightInMeters)).toFixed(2);
      setBMI(bmiValue);
      determineBMICategory(bmiValue);
    }
  };

  const determineBMICategory = (bmiValue) => {
    if (bmiValue < 18.5) {
      setBMICategory('Underweight');
    } else if (bmiValue >= 18.5 && bmiValue < 24.9) {
      setBMICategory('Normal Weight');
    } else if (bmiValue >= 25 && bmiValue < 29.9) {
      setBMICategory('Overweight');
    } else {
      setBMICategory('Obese');
    }
  };
  

  return (
    <div className='container'>
 <p>
        Body Mass Index (BMI) is a numerical value calculated from a person's weight and height. It is a reliable
        indicator of body fatness for most people and is used to screen for weight categories that may lead to health
        problems. To calculate your BMI, enter your height and weight below and click "Calculate BMI".
      </p>
      <div className="input-group">
        <label>Height (cm):</label>
        <input type="number" value={height} onChange={(e) => setHeight(e.target.value)} />
      </div>
      <div className="input-group">
        <label>Weight (kg):</label>
        <input type="number" value={weight} onChange={(e) => setWeight(e.target.value)} />
      </div>
      <button onClick={calculateBMI}>Calculate BMI</button>
      {bmi && (
        <div className="result">
          <p>Your BMI: {bmi}</p>
          <p>Category: {bmiCategory}</p>
          <p>
            BMI Categories:
            <ul>
              <li>Underweight: BMI less than 18.5</li>
              <li>Normal Weight: BMI between 18.5 and 24.9</li>
              <li>Overweight: BMI between 25 and 29.9</li>
              <li>Obese: BMI 30 or higher</li>
            </ul>
          </p>
        </div>
      )}
    </div>
  );
};

export default BMICalculator;
