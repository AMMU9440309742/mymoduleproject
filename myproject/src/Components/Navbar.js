
import React, { useState, useEffect } from 'react';
import { Navbar, Nav, Container, Collapse ,Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FaUserPlus, FaBars, FaWhatsapp, FaInstagram, FaFacebook, FaEnvelope } from 'react-icons/fa';
// import logo from '../images/images/logo.png';
import logo1 from '../images/images/logo1.jpg';
import logo2 from '../images/images/logo2.jpg';
import logo3 from '../images/images/logo3.jpg';
import background3 from '../images/images/background3.jpg';
import background2 from '../images/images/background2.jpg';
import img1 from '../images/images/img1.jpg';

const MyNavbar = () => {
    const [showBackground3, setShowBackground3] = useState(true);
    const [openNav, setOpenNav] = useState(false);

    useEffect(() => {
        const interval = setInterval(() => {
            setShowBackground3(prevState => !prevState);
        }, 5000);

        return () => clearInterval(interval);
    }, []);

    const baseBgStyle = {
        backgroundSize: '200% 100%',
        backgroundRepeat: 'no-repeat',
        minHeight: '100vh',
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: -1,
        animation: 'slideBackground 10s linear infinite'
    };

    const bg3Style = {
        ...baseBgStyle,
        backgroundImage: `url(${background3})`
    };

    const bg2Style = {
        ...baseBgStyle,
        backgroundImage: `url(${background2})`,
        animationDelay: '5s'
    };

    const navbarStyle = {
        backgroundColor: openNav ? '#001F3F' : 'transparent',
        width: '250px',
        height: '100%',
        position: 'fixed',
        top: 0,
        left: 0,
        zIndex: 1,
        boxShadow: openNav ? '0px 0px 15px rgba(0, 0, 0, 0.1)' : 'none'
    };

    const brandStyle = {
        fontSize: '20px',
        fontWeight: 'bold',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: '1rem',
        color: openNav ? '#FFFFFF' : '#333'
    };

    const navLinkStyle = {
        color: '#FFFFFF',
        marginLeft: '15px',
        marginRight: '15px',
        fontWeight: '700',
        transition: 'color 0.3s',
        fontSize: '19px',
        padding: '0.5rem 0'
    };

    const roundedLogoStyle = {
        width: '80px',
        borderRadius: '50%'  // This makes the image rounded
    };

    return (
        <div>
            <div style={bg3Style}></div>
            <div style={bg2Style}></div>

            {/* Navbar */}
            <div style={navbarStyle}>
                <Container fluid>
                    <Navbar.Brand as={Link} to="/" style={brandStyle} onClick={() => setOpenNav(!openNav)}>
                        <FaBars style={{color: openNav ? '#FFFFFF' : '#333', marginRight: '10px'}} />
                        Health & Wellness
                    </Navbar.Brand>
                    <Collapse in={openNav}>
                        <Nav className="flex-column">
                            <Nav.Link as={Link} to="/" style={navLinkStyle} activeStyle={{color: 'red'}}>Home</Nav.Link>
                            <Nav.Link as={Link} to="/Register" style={navLinkStyle} activeStyle={{color: 'red'}}>Sign-up</Nav.Link>
                            <Nav.Link as={Link} to="/Login" style={navLinkStyle} activeStyle={{color: 'red'}}>Sign-in</Nav.Link>
                            <Nav.Link as={Link} to="/bmi" style={navLinkStyle} activeStyle={{color: 'red'}}>Calculate your BMI</Nav.Link>
                            <Nav.Link as={Link} to="/Health" style={navLinkStyle} activeStyle={{color: 'red'}}>Check your Health</Nav.Link>
                        
                            <Nav.Link as={Link} to="/medicine" style={navLinkStyle} activeStyle={{color: 'red'}}>Search for medicine</Nav.Link>
                            <Nav.Link as={Link} to="/Contact" style={navLinkStyle} activeStyle={{color: 'red'}}>Contact</Nav.Link>
                            
                            
                        </Nav>
                    </Collapse>
                </Container>
            </div>

            {/* Main Content */}
            <div style={{ marginLeft: '270px', paddingTop: '2rem' }}>
                {/* Banner */}
                <div style={{ textAlign: 'center', marginBottom: '20px' }}>
                    <img src={img1} alt="Medical Products" style={{ maxWidth: '60%', borderRadius: '50%', boxShadow: '0px 5px 15px rgba(0, 0, 0, 0.2)' }} />
                </div>

                {/* Categories */}
                <div style={{ display: 'flex', justifyContent: 'space-around', marginBottom: '20px' }}>
                    <div>
                        <h3>Medicines</h3>
                        <img src={logo1} alt="Medicines" style={roundedLogoStyle} />
                    </div>
                    <div>
                        <h3>Wellness</h3>
                        <img src={logo2} alt="Wellness" style={roundedLogoStyle} />
                    </div>
                    <div>
                        <h3>Supplements</h3>
                        <img src={logo3} alt="Supplements" style={roundedLogoStyle} />
                    </div>
                </div>

                {/* Offers & Discounts */}
                <div style={{ textAlign: 'center', marginBottom: '20px', color: 'black', fontWeight: 'bold', fontSize: '40px' }}>
                    <h2>Special Offers & Discounts</h2>
                    <p>Grab the latest deals and enjoy up to 50% off on selected items!</p>
                </div>
                <div style={{ textAlign: 'center', marginBottom: '20px' }}>
            <Link to="/Register">
                <Button variant="warning" size="sm" style={{ width: '150px', display: 'inline-block' }}>
                    <FaUserPlus style={{ marginRight: '10px' }} />
                    Register Here
                </Button>
            </Link>
        </div>


                {/* Testimonials */}
                <div style={{ textAlign: 'center', marginBottom: '20px', fontWeight: 'bold', fontSize: '30px' }}>
                    <h2>What Our Customers Say</h2>
                    <p>"The quality of medicines is top-notch. Highly recommended!" - John Doe</p>
                    <p>"Fast delivery and excellent customer service." - Jane Smith</p>
                </div>

                {/* FAQ */}
                <div style={{ marginBottom: '20px', fontWeight: 'bold', fontSize: '30px', color: 'darkgray' }}>
                    <h2>Frequently Asked Questions</h2>
                    <p><strong>Q:</strong> How fast is the delivery?</p>
                    <p><strong>A:</strong> We provide next-day delivery for all orders placed before 5 PM.</p>
                </div>

                {/* Footer */}
                <footer style={{ backgroundColor: '#001F3F', padding: '1rem', color: 'white', textAlign: 'center', marginTop: '20px' }}>
                    <div style={{ marginBottom: '1rem' }}>
                        <a href="https://chat.whatsapp.com/invite/H6ej8BTHK360oP2EXWDhdF" target="_blank" rel="noopener noreferrer" style={{ margin: '0 10px', color: 'white' }}><FaWhatsapp size={25} /></a>
                        <a href="https://www.instagram.com/pharmacitamal/" target="_blank" rel="noopener noreferrer" style={{ margin: '0 10px', color: 'white' }}><FaInstagram size={25} /></a>
                        <a href="https://www.facebook.com/mymedicalshop.official/" target="_blank" rel="noopener noreferrer" style={{ margin: '0 10px', color: 'white' }}><FaFacebook size={25} /></a>
                        <a href="https://support.google.com/adspolicy/answer/176031?hl=en" style={{ margin: '0 10px', color: 'white' }}><FaEnvelope size={25} /></a>
                    </div>
                    <p>Contact Us: +919440309742 | Email: pharmamedicalstore45@gmail.com</p>
                </footer>
            </div>
        </div>
    );
}

// Inject CSS animation and hover effect into the document
const styleSheet = `
@keyframes slideBackground {
    0%, 100% {
        background-position: 100% 50%;
    }
    50% {
        background-position: 0% 50%;
    }
}

a.nav-link:hover {
    color: #ffa07a !important;
}

.custom-btn-hover:hover {
    background-color: #ffa07a !important;
    color: #001F3F !important;
}
`;


document.head.appendChild(document.createElement("style")).innerHTML = styleSheet;

export default MyNavbar;
