import React, { useEffect } from 'react';

const RazorpayButton = ({ paymentButtonId }) => {
    useEffect(() => {
        const form = document.createElement('form');
        form.id = paymentButtonId + '_form';
        document.body.appendChild(form);

        const script = document.createElement('script');
        script.src = 'https://checkout.razorpay.com/v1/payment-button.js';
        script.async = true;
        script.setAttribute('data-payment_button_id', paymentButtonId);

        form.appendChild(script);

        return () => {
            document.body.removeChild(form);
        };
    }, [paymentButtonId]);

    return <div id={paymentButtonId}></div>;
};

export default RazorpayButton;
