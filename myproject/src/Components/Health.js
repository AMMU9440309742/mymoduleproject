import React, { useState } from 'react';
import './Health.css'; // Import your CSS file if styles are in a separate file

const HealthMonitor = () => {
  const [temperature, setTemperature] = useState('');
  const [bloodPressure, setBloodPressure] = useState('');
  const [bloodSugar, setBloodSugar] = useState('');
  const [headache, setHeadache] = useState(false);

  const checkHealth = () => {
    // Implement health checking logic based on entered data
    // You can use if statements or other logic to assess the user's health
    // For simplicity, let's assume that if the user has a headache and high temperature, it indicates a health issue
    if (headache && parseFloat(temperature) > 37.5) {
      alert('Please consult a doctor. You might be experiencing a health issue.');
    } else {
      alert('You seem to be fine, but if you have concerns, consult a doctor.');
    }
  };

  return (
    <div className="containerStyle">
      <div className="health-monitor-container">
        <h2>Health Monitor</h2>
        <div className="input-group">
          
          <label>Body Temperature (°C):</label>
          <input type="number" value={temperature} onChange={(e) => setTemperature(e.target.value)} />
        </div>
        <div className="input-group">
          <label>Blood Pressure (mmHg):</label>
          <input type="text" value={bloodPressure} onChange={(e) => setBloodPressure(e.target.value)} />
        </div>
        <div className="input-group">
          <label>Blood Sugar (mg/dL):</label>
          <input type="text" value={bloodSugar} onChange={(e) => setBloodSugar(e.target.value)} />
        </div>
        <div className="input-group">
          <label>Do you have a headache?</label>
          <input type="checkbox" checked={headache} onChange={() => setHeadache(!headache)} />
        </div>
        <button onClick={checkHealth}>Check Health</button>
      </div>
    </div>
  );
};

export default HealthMonitor;
