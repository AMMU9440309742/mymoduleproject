// import React from 'react';
// import './Footer.css';

// const Footer = () => {
//     return (
//         <footer className="footer">
//             <div className="social-icons">
//                 {/* Add your social media icons here */}
//                 <i className="fab fa-facebook-f"></i>
//                 <i className="fab fa-twitter"></i>
//                 <i className="fab fa-instagram"></i>
//                 {/* Add more social media icons as needed */}
//             </div>
//             <div className="footer-info">
//                 <p>Online Medical Store</p>
//                 <p>123 Medical Street, Health City</p>
//                 <p>Phone: 123-456-7890</p>
//                 <p>Email: info@medicalstore.com</p>
//             </div>
//         </footer>
//     );
// }

// export default Footer;

import React from 'react';
import '@fortawesome/fontawesome-free/css/all.min.css'; // Import Font Awesome CSS
import './Footer.css';

const Footer = () => {
    return (
        <footer className="footer">
            <div className="social-icons">
                <i className="fab fa-facebook-f"></i>
                <i className="fab fa-twitter"></i>
                <i className="fab fa-instagram"></i>
                {/* Add more social media icons as needed */}
            </div>
            <div className="footer-info">
                <p>Online Medical Store</p>
                <p>123 Medical Street, Health City</p>
                <p>Phone: 123-456-7890</p>
                <p>Email: info@medicalstore.com</p>
            </div>
            </footer>
    );
}

export default Footer;
