import React, { useState } from 'react';
import './Contact.css';
import './Footer.css';
import '@fortawesome/fontawesome-free/css/all.min.css';


const Contact = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [subject, setSubject] = useState('General Inquiry');
    const [message, setMessage] = useState('');
    const [attachment, setAttachment] = useState(null);
    const [formSubmitted, setFormSubmitted] = useState(false);
    const [formError, setFormError] = useState('');

    const handleSubmit = async e => {
        e.preventDefault();

        // Form validation logic here (validate email, attachment format, etc.)
        // For example, you can check if the email is in a valid format and if the attachment format is supported.

        // Prepare form data to send to the server
        const formData = new FormData();
        formData.append('name', name);
        formData.append('email', email);
        formData.append('subject', subject);
        formData.append('message', message);
        if (attachment) {
            formData.append('attachment', attachment);
        }

        try {
            // Send form data to the server
            const response = await fetch('YOUR_API_ENDPOINT', {
                method: 'POST',
                body: formData,
            });

            if (response.ok) {
                // Handle success
                console.log('Form submitted successfully!');
                setFormSubmitted(true);
            } else {
                // Handle server errors
                console.error('Form submission failed.');
                setFormError('Failed to submit the form. Please try again later.');
            }
        } catch (error) {
            // Handle network errors or other exceptions
            console.error('Error occurred while submitting the form:', error);
            setFormError('Failed to submit the form. Please check your internet connection and try again.');
        }
    };

    const handleFileChange = e => {
        const file = e.target.files[0];
        setAttachment(file);
    };

    return (
        <div className="contact-container">
            <div className="contact-form">
                <h2>Contact Information</h2>
                {formSubmitted ? (
                    <div className="success-message">
                        Thank you for contacting us! We will get back to you shortly.
                    </div>
                ) : (
                    <form onSubmit={handleSubmit}>
                        <div className="input-group">
                            <label>Name:</label>
                            <input type="text" value={name} onChange={e => setName(e.target.value)} required />
                        </div>
                        <div className="input-group">
                            <label>Email:</label>
                            <input type="email" value={email} onChange={e => setEmail(e.target.value)} required />
                        </div>
                        <div className="input-group">
                            <label>Subject:</label>
                            <input type="text" value={subject} onChange={e => setSubject(e.target.value)} required />
                        </div>
                        <div className="input-group">
                            <label>Message:</label>
                            <textarea value={message} onChange={e => setMessage(e.target.value)} required />
                        </div>
                        <div className="input-group">
                            <label>Attachment:</label>
                            <input type="file" onChange={handleFileChange} accept=".pdf,.doc,.docx,.jpg,.jpeg,.png" />
                        </div>
                        {formError && <div className="error-message">{formError}</div>}
                        <button type="submit">Submit</button>
                    </form>
                )}
            </div>
            <div className="footer">
                <div className="social-icons">
                    <i className="fab fa-facebook-f"></i>
                    <i className="fab fa-twitter"></i>
                    <i className="fab fa-instagram"></i>
                    
                </div>
                <div className="footer-info">
                    <p>Online Medical Store</p>
                    <p>123 Medical Street, Health City</p>
                    <p>Phone: 123-456-7890</p>
                    <p>Email: info@medicalstore.com</p>
                </div>
            </div>
        </div>
    );
}

export default Contact;
